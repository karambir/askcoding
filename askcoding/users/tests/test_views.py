# -*- coding: utf-8 -*-

# Third Party
import pytest
from django.core.urlresolvers import reverse

# AskCoding
from askcoding.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


@pytest.fixture
def user():
    return UserFactory.create()


def test_get_user_profile_non_logged_in(client):
    url = reverse('account_profile')
    response = client.get(url)
    assert response.status_code == 200
    assert 'login' in response.content.decode('utf-8')


def test_get_user_profile_logged_in(client, user):
    url = reverse('account_profile')
    client.login(email=user.email, password='test')
    response = client.get(url)
    assert response.status_code == 200
    assert user.name in response.content.decode('utf-8')


def test_get_user_profile_update(client, user):
    url = reverse('account_profile_update')
    client.login(email=user.email, password='test')
    response = client.get(url)
    assert response.status_code == 200
    assert user.name in response.content.decode('utf-8')


def test_post_user_profile_update(client, user):
    url = reverse('account_profile_update')
    client.login(email=user.email, password='test')
    new_name = 'something new'
    data = {
        'name': new_name,
    }
    response = client.post(url, data, follow=True)
    assert response.status_code == 200
    assert new_name in response.content.decode('utf-8')
