# -*- coding: utf-8 -*-.

# Third Party
from django.forms import ModelForm

from .models import User


class UserProfileForm(ModelForm):

    class Meta:
        model = User
        fields = ['name', ]
