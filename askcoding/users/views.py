# -*- coding: utf-8 -*-.

# Third Party
from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.views.generic.edit import UpdateView

from .forms import UserProfileForm


class UserProfileUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'profile/form.html'
    form_class = UserProfileForm

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('account_profile')

user_profile_update = UserProfileUpdateView.as_view()
